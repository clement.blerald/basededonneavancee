# Choix pour la modélisation

## Les checks

- date_fin < date_début sur les tables contrats

- check sur catégorie, sur la table réalisation
ex : vérifie que celui qui insère un album soit un musicien

- check sur nom_métier et instruments/Style de musique, sur la table Artiste
ex : vérifie que seul les musiciens peuvent jouer d'un instrument

- check sur contrats : il ne doit pas avoir de contrat en cours quand on en insère un autre
- check sur les clefs étrangère de la table contact : on ne se contacte pas soi-même 
- check sur audition : l'artiste ne doit pas déjà avoir été proposé pour le projet
- check sur demande : l'attribut job doit correspondre à un métier possible (attribut nom_metier de Artiste)

## Les triggers 

- insertion/update succès, => update sur contrat-AP => insert dans realisation
- sur insertion/update contratsAP => insertion dans versement
- insertion/update dans versement => insertion dans comptabilité
- insertion sur table accepter => insertion dans table refuser de tout les artistes qui était à l'audition sauf l'artiste accepté
- après envoie sur demande, insert dans audition
- insertion sur agent, update sur artiste
- insertion dans management => insert dans audition
- insertion dans nom/langue demande => delete dans audition
- insertion dans demande => insertion dans audition
- insertion dans agent => insertion dans management (si possible)
- insertion dans audition => check respect de la demande


## Contrainte uniques

***Rq***: De manière générale aucune de nos tables n'acceptent les doublons (en ignorant l'id)

- unique(titre) sur la table réalisation
- unique(nom_evenement,prix_obtenu, date) sur la table récompenses
- unique(auteur_critique,clef étrangère id_réalisation)
- UNIQUE(id_personne,nom_metier)
- UNIQUE(id_demande,id_artiste)
- UNIQUE(id_demande) (dans nom_demande)
- UNIQUE (id,id_agence) (dans agent)
- UNIQUE(id_audition) (dans accepter)
- UNIQUE(id_audition,id_artiste) (dans refuser)


--- 

## Choix de table 

- accepter
- refuser
- audition
- demande
- artiste
- langue
- langue_parle
- langue_demande
- nom_demande
- réalisation
- agent
- agence
- parle
- personne
- management
