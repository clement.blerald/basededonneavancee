/* CREATION DES TABLES */

DROP TABLE IF EXISTS artiste CASCADE;
DROP TABLE IF EXISTS personne CASCADE;
DROP TABLE IF EXISTS accepter CASCADE;
DROP TABLE IF EXISTS refuser CASCADE;
DROP TABLE IF EXISTS audition CASCADE;
DROP TABLE IF EXISTS demande CASCADE;
DROP TABLE IF EXISTS langue CASCADE;
DROP TABLE IF EXISTS langue_demande CASCADE;
DROP TABLE IF EXISTS nom_demande CASCADE;
DROP TABLE IF EXISTS realisation CASCADE;
DROP TABLE IF EXISTS agent CASCADE;
DROP TABLE IF EXISTS agence CASCADE;
DROP TABLE IF EXISTS langue_parle CASCADE;
DROP TABLE IF EXISTS management CASCADE;

CREATE TABLE personne (
    id serial PRIMARY KEY,
    nom TEXT,
    prenom TEXT,
    sex TEXT,
    date_naissance TIMESTAMP,
    CHECK (sex IN('Male','Female','Neutre'))
);

CREATE TABLE artiste(
    id SERIAL PRIMARY KEY,
    id_personne INT,
    nom_metier TEXT,
    FOREIGN KEY (id_personne) REFERENCES personne(id),
    CHECK (nom_metier IN ('musicien','comédien','danseur')),
    UNIQUE(id_personne,nom_metier)
);

CREATE TABLE agence(
    id serial PRIMARY KEY,
    nom TEXT,
    pays TEXT,
    ville TEXT
);

CREATE TABLE langue(
    id serial PRIMARY KEY,
    nom TEXT
);

CREATE TABLE langue_parle(
    id_langue INT,
    id_personne INT,
    FOREIGN KEY (id_langue) REFERENCES langue(id),
    FOREIGN KEY (id_personne) REFERENCES personne(id),
    PRIMARY KEY (id_langue,id_personne)
);

CREATE TABLE demande(
    id serial PRIMARY KEY,
    nom_projet TEXT,
    id_agence INT,
    date_demande TIMESTAMP,
    metier TEXT,
    deb_periode TIMESTAMP,
    fin_periode TIMESTAMP,
    sex TEXT DEFAULT 'Non_preciser',
    FOREIGN KEY (id_agence) REFERENCES agence(id),
    CHECK (metier IN ('musicien','comédien','danseur')), 
    CHECK (deb_periode < fin_periode),
    CHECK (sex IN ('Neutre','Female','Male','Non_preciser'))
);

CREATE TABLE realisation(
    id serial PRIMARY KEY,
    titre TEXT,
    date_real TIMESTAMP,
    type_role TEXT,
    id_personne INT,
    FOREIGN KEY (id_personne) REFERENCES personne(id),
    CHECK (type_role IN ('musicien','comédien','danseur')),
    UNIQUE (titre,id_personne)
);

CREATE TABLE audition(
    id serial PRIMARY KEY,
    id_demande INT,
    id_artiste INT,
    FOREIGN KEY (id_demande) REFERENCES demande(id),
    FOREIGN KEY (id_artiste) REFERENCES artiste(id),
    UNIQUE(id_demande,id_artiste)
);

CREATE TABLE langue_demande(
    langue_id INT,
    demande_id INT,
    FOREIGN KEY (langue_id) REFERENCES langue(id),
    FOREIGN KEY (demande_id) REFERENCES demande(id),
    PRIMARY KEY (langue_id,demande_id)
);

CREATE TABLE nom_demande(
    id_demande INT,
    prenom_artiste TEXT,
    nom_artiste TEXT,
    UNIQUE(id_demande)
);

CREATE TABLE agent(
    id INT PRIMARY KEY,
    id_agence INT,
    FOREIGN KEY (id_agence) REFERENCES agence(id),
    FOREIGN KEY (id) REFERENCES personne(id),
    UNIQUE (id,id_agence)
);

CREATE TABLE management(
    id_agent INT,
    id_artiste INT,
    FOREIGN KEY (id_artiste) REFERENCES artiste(id),
    FOREIGN KEY (id_agent) REFERENCES agent(id),
    PRIMARY KEY(id_agent,id_artiste)
);

CREATE TABLE accepter(
    id_audition INT,
    id_artiste INT,
    FOREIGN KEY (id_artiste) REFERENCES artiste(id),
    FOREIGN KEY (id_audition) REFERENCES audition(id),
    UNIQUE(id_audition)
);

CREATE TABLE refuser(
    id_audition INT,
    id_artiste INT,
    FOREIGN KEY (id_artiste) REFERENCES artiste(id),
    FOREIGN KEY (id_audition) REFERENCES audition(id),
    UNIQUE(id_audition,id_artiste)
);

/* CREATION DES INDEXS */

DROP INDEX IF EXISTS index_personne;
DROP INDEX IF EXISTS index_artiste;
DROP INDEX IF EXISTS index_demande;
DROP INDEX IF EXISTS index_realisation;
DROP INDEX IF EXISTS index_agence;
DROP INDEX IF EXISTS index_demande_date;

CREATE INDEX index_personne ON personne(id);
CREATE INDEX index_artiste ON artiste(id);
CREATE INDEX index_demande ON demande(id);
CREATE INDEX index_realisation ON realisation(id);
CREATE INDEX index_agence ON agence(id);
CREATE INDEX index_demande_date ON demande(deb_periode,fin_periode); 