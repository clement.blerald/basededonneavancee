DROP TRIGGER IF EXISTS clore_audition ON accepter;
DROP TRIGGER IF EXISTS trouver_travail ON agent;
DROP TRIGGER IF EXISTS insert_audition ON demande;
DROP TRIGGER IF EXISTS delete_audition_langue ON langue_demande;
DROP TRIGGER IF EXISTS delete_audition_personne ON nom_demande;
DROP TRIGGER IF EXISTS realisation_insertion ON accepter;
DROP TRIGGER IF EXISTS check_agent_pas_artiste ON agent;
DROP TRIGGER IF EXISTS check_artiste_pas_agent ON artiste;
DROP TRIGGER IF EXISTS check_insert_audition ON audition;
DROP TRIGGER IF EXISTS management_insertion ON management;
DROP TRIGGER IF EXISTS artiste_a_agent ON management;

CREATE OR REPLACE FUNCTION refuser_artiste() RETURNS TRIGGER AS $$ 
DECLARE 
    liste_artiste artiste%ROWTYPE;
    artiste_deja_refuse artiste%ROWTYPE;
    id_dem INT;
BEGIN
    RAISE NOTICE 'Dans procédure : refuser_artiste => refuser les artistes pour une demande lorsque l un d entre eux est accepté';

    /*on cherche l'id de la demande associé à l'audition*/
    SELECT au.id_demande
    INTO id_dem
    FROM audition as au
    WHERE NEW.id_artiste = au.id_artiste;
    RAISE NOTICE 'L id de la demande associé a l audition est %', id_dem;

    FOR liste_artiste IN (SELECT au.id_artiste
    FROM audition AS au 
    WHERE 
    id_dem = au.id_demande
    AND NEW.id_artiste != au.id_artiste
    AND au.id_artiste NOT IN(
        SELECT id_artiste
        FROM refuser
        WHERE id_audition IN
        (SELECT id FROM audition WHERE id_demande = id_dem)
        )
    )
    LOOP
        INSERT INTO refuser(id_audition,id_artiste) VALUES (
            (SELECT id FROM audition WHERE id_demande = id_dem AND id_artiste = liste_artiste.id)
            ,liste_artiste.id);
    END LOOP;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER clore_audition
AFTER
INSERT ON accepter FOR EACH row
EXECUTE PROCEDURE refuser_artiste();


CREATE OR REPLACE FUNCTION trouver_artiste_seul() RETURNS TRIGGER AS $$ 
DECLARE
    artiste_seul    artiste%ROWTYPE;
    agent_double    artiste%ROWTYPE;
BEGIN
    RAISE NOTICE 'Dans procédure : trouver_artiste_seul';
    
    /*récupère tous les artistes seul*/
    SELECT id
    INTO artiste_seul
    FROM artiste
    WHERE id NOT IN (
        SELECT id_artiste
        FROM management
    ) LIMIT 1;

    IF NOT FOUND THEN 
    RAISE NOTICE 'Tous les artistes connu possèdent déjà un agent';
    RETURN NULL;
    END IF;
    
    INSERT INTO management
    VALUES(NEW.id,artiste_seul.id);

    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trouver_travail
AFTER
INSERT ON agent FOR EACH row
EXECUTE PROCEDURE trouver_artiste_seul();

CREATE OR REPLACE FUNCTION check_agent_pas_artiste_func() RETURNS TRIGGER AS $$ 
DECLARE
    agent_double artiste%ROWTYPE;
BEGIN
    RAISE NOTICE 'Dans procédure : trouver_travail => cherche un artiste libre pour l assigner au nouvel agent';
    
    SELECT * INTO agent_double FROM artiste WHERE id_personne = NEW.id;

    IF NOT FOUND THEN
        RAISE NOTICE 'L agent N EST PAS un artiste';
    ELSE
        RAISE NOTICE 'L agent est DEJA un artiste';
        RETURN NULL;
    END IF;

    RAISE NOTICE 'RAS => tuple INSERE';
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER check_agent_pas_artiste
BEFORE
INSERT ON agent FOR EACH row
EXECUTE PROCEDURE check_agent_pas_artiste_func();

CREATE OR REPLACE FUNCTION check_artiste_pas_agent_func() RETURNS TRIGGER AS $$ 
DECLARE
    agent_double INT;
BEGIN
    RAISE NOTICE 'Dans procédure : check_artiste_pas_agent';
    
    SELECT * INTO agent_double FROM agent WHERE id = NEW.id_personne;

    IF NOT FOUND THEN
        RAISE NOTICE 'L artiste N EST PAS un agent';
    ELSE
        RAISE NOTICE 'L artiste est DEJA un agent';
        RETURN NULL;
    END IF;

    RAISE NOTICE 'RAS => tuple INSERE';
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER check_artiste_pas_agent
BEFORE
INSERT ON artiste FOR EACH row
EXECUTE PROCEDURE check_artiste_pas_agent_func();

CREATE OR REPLACE FUNCTION deja_agent() RETURNS TRIGGER AS $$ 
DECLARE
    deja_agent management%ROWTYPE;
BEGIN
    RAISE NOTICE 'Dans procédure : deja_agent';

    SELECT id_agent INTO deja_agent FROM management WHERE id_artiste IN(
        SELECT id FROM artiste WHERE id_personne IN(
            SELECT id FROM personne where id IN(
                SELECT id_personne FROM artiste WHERE id = NEW.id_artiste))) LIMIT 1; 

    IF FOUND THEN
        RAISE NOTICE 'L artiste a déja un agent';
        NEW.id_agent := deja_agent.id_agent;
    END IF;
    
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER artiste_a_agent
BEFORE
INSERT ON management FOR EACH row
EXECUTE PROCEDURE deja_agent();

CREATE OR REPLACE FUNCTION audition_in() RETURNS TRIGGER AS $$ 
DECLARE
    liste_artiste artiste%ROWTYPE;
BEGIN
    RAISE NOTICE 'Dans procédure : audition_in recherche d audition pour un artiste';
    FOR 
    liste_artiste IN (
        SELECT id
        FROM artiste
        WHERE id_personne IN
            (SELECT id 
            FROM personne
            WHERE id IN
                (SELECT DISTINCT artiste.id_personne
                        FROM artiste
                        WHERE artiste.id in(
                            (SELECT DISTINCT id_artiste
                                FROM management 
                                WHERE id_agent in 
                                (SELECT agent.id
                                    FROM agent 
                                    WHERE NEW.id_agence = agent.id_agence))))
            AND sex = NEW.sex OR NEW.sex = 'Non_preciser')) 
    LOOP
        INSERT INTO audition(id_demande,id_artiste) VALUES (NEW.id,liste_artiste.id);
    END LOOP;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER insert_audition
AFTER
INSERT ON demande FOR EACH row
EXECUTE PROCEDURE audition_in();

CREATE OR REPLACE FUNCTION audition_langue() RETURNS TRIGGER AS $$ 
DECLARE
    liste_artiste artiste%ROWTYPE;
BEGIN
    RAISE NOTICE 'Dans procédure : audition_langue delete de audition les personnes ne parlant pas la langue voulue';
    FOR 
    liste_artiste IN (
        SELECT id
        FROM artiste
        WHERE id_personne IN(
            SELECT id_personne
            FROM langue_parle
            WHERE id_personne IN(
                SELECT id_personne 
                FROM artiste
                WHERE id IN
                    (SELECT id_artiste
                    FROM audition
                    WHERE id_artiste IN(
                        SELECT DISTINCT id_artiste
                        FROM artiste,audition
                        WHERE id_personne IN
                            (SELECT id_personne
                            FROM langue_parle
                            WHERE artiste.id_personne = id_personne))
                    AND audition.id_demande = NEW.demande_id))
            AND id_langue != NEW.langue_id))
    LOOP
        DELETE FROM audition WHERE id_artiste=liste_artiste.id AND audition.id_demande = NEW.demande_id;
    END LOOP;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_audition_langue
AFTER
INSERT ON langue_demande FOR EACH row
EXECUTE PROCEDURE audition_langue();

CREATE OR REPLACE FUNCTION audition_personne() RETURNS TRIGGER AS $$ 
DECLARE
    liste_artiste artiste%ROWTYPE;
BEGIN
    RAISE NOTICE 'Dans procédure : audition_personne delete de audition les personnes ne possedant pas se nom et prenom';
    FOR 
    liste_artiste IN (
        SELECT id
		FROM artiste
		WHERE id_personne IN(
            SELECT id
            FROM personne
            WHERE id IN(
                SELECT id_personne 
                FROM artiste
                WHERE id IN
                    (SELECT id_artiste
                    FROM audition
                    WHERE id_artiste IN(
                        SELECT DISTINCT id_artiste
                        FROM artiste,audition
                        WHERE id_personne IN
                            (SELECT id_personne
                            FROM langue_parle
                            WHERE artiste.id_personne = id_personne))
                    AND audition.id_demande = NEW.id_demande))
            AND nom != NEW.nom_artiste
			AND prenom != NEW.prenom_artiste))
    LOOP
        DELETE FROM audition WHERE id_artiste=liste_artiste.id AND audition.id_demande = NEW.id_demande;
    END LOOP;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_audition_personne
AFTER
INSERT ON nom_demande FOR EACH row
EXECUTE PROCEDURE audition_personne();

CREATE OR REPLACE FUNCTION new_realisation() RETURNS TRIGGER AS $$ 
DECLARE
    liste_demande demande%ROWTYPE;
    personne_id integer;
BEGIN
    RAISE NOTICE 'Dans procédure : new_realisation => insère une réalisation lors d un ajout dans accepter';
    SELECT id_personne 
    INTO personne_id
    FROM artiste
    WHERE id IN(
        SELECT DISTINCT id_artiste
        FROM accepter
        WHERE accepter.id_audition = NEW.id_audition);
	RAISE NOTICE 'L id de la personne associé a l audition est %', personne_id;

	FOR
    liste_demande IN(   
		SELECT *
		FROM demande
		WHERE id IN(
			SELECT id_demande
			FROM audition
			WHERE id IN(
				SELECT DISTINCT id_audition
				FROM accepter
				WHERE accepter.id_audition = NEW.id_audition)))
    LOOP
        INSERT INTO realisation(titre,date_real,type_role,id_personne) VALUES (liste_demande.nom_projet,liste_demande.deb_periode,liste_demande.metier,personne_id);
    END LOOP;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER realisation_insertion
AFTER
INSERT ON accepter FOR EACH row
EXECUTE PROCEDURE new_realisation();


/* check dans langue_demande et nom_demande  */

CREATE OR REPLACE FUNCTION check_aud() RETURNS TRIGGER AS $$ 
DECLARE
    demande_associe record;
    personne_info personne%ROWTYPE;
    metier_info artiste%ROWTYPE;
    liste_langue_demande langue%ROWTYPE;
    langue_parle_artiste langue%ROWTYPE;
    liste_nom_demande record;
    trouve management%ROWTYPE;
    nomA text;
    prenomA text;
BEGIN
    SELECT * 
    INTO demande_associe
    FROM demande
    WHERE id = NEW.id_demande;

    IF NOT FOUND THEN
    RAISE NOTICE 'Cette audition : num° %, ne correspond à aucune demande',NEW.id;
    RETURN NULL;
    END IF;

    RAISE NOTICE 'La demande associé est %', to_json(demande_associe);
    RAISE NOTICE 'On veut insérer %', to_json(NEW);
    
    SELECT * INTO personne_info FROM personne WHERE id IN
    (SELECT id_personne FROM artiste WHERE id = NEW.id_artiste);
    /*trouve les infos de l'artiste qu'on va insérer*/
    SELECT * INTO metier_info FROM artiste WHERE id_personne IN
        (SELECT id FROM personne WHERE id IN
            (SELECT id_personne FROM artiste WHERE id = NEW.id_artiste));

    /*check le sex*/
    IF (personne_info.sex != demande_associe.sex AND demande_associe.sex != 'Non_preciser') THEN
        RAISE NOTICE 'il y a une incohérence au niveau du sex, tuple NON INSERE';
        RAISE NOTICE'personne_info sex : %',personne_info.sex;
        RAISE NOTICE 'demande_associe.sex : %',demande_associe.sex;
        RETURN NULL;
    END IF;
    /*check le métier*/
    IF (demande_associe.metier NOT IN (metier_info.nom_metier) ) THEN
        RAISE NOTICE 'il y a une incohérence au niveau du métier, tuple NON INSERE';
        RAISE NOTICE 'metier_info.nom_metier : %',metier_info.nom_metier;
        RAISE NOTICE 'demande_associe.metier : %',demande_associe.metier;
        RETURN NULL;
    END IF;
    /*check langue*/
    SELECT * INTO liste_langue_demande FROM langue WHERE id IN(
        SELECT langue_id
        FROM langue_demande
        WHERE langue_demande.demande_id = NEW.id_demande
    );

    IF FOUND THEN
        SELECT * INTO langue_parle_artiste FROM langue WHERE id IN(
            SELECT id_langue FROM langue_parle
            WHERE langue_parle.id_personne IN(SELECT id FROM personne WHERE id IN (SELECT id_personne FROM artiste WHERE id = NEW.id_artiste))
        );
        IF liste_langue_demande NOT IN (langue_parle_artiste) THEN
            RAISE NOTICE 'l artiste ne parle pas toutes les langues demandées';
            RETURN NULL;
        END IF;
    END IF;

    /*check nom*/
    SELECT prenom_artiste,nom_artiste INTO prenomA,nomA FROM nom_demande WHERE id_demande = NEW.id_demande;
    
    IF FOUND THEN
        RAISE NOTICE 'ICI | nom :  %, prenom :  %',nomA,prenomA;
        SELECT id_personne FROM personne WHERE nom = nomA AND prenom = prenomA AND id IN (SELECT id_personnne FROM artiste WHERE id = NEW.id_artiste);
        IF NOT FOUND THEN
            RAISE NOTICE 'L artiste n a pas le bon nom';
            RETURN NULL;
        END IF;
    END IF;

    /* check si l'artiste fait bien parti de l'agence ou est adressé la demande */
    
    SELECT *
    INTO trouve
    FROM management
    WHERE id_artiste = NEW.id_artiste AND id_agent in (select id from agent where id_agence = demande_associe.id_agence);

    IF NOT FOUND THEN
        RAISE NOTICE 'l artiste à insérer ne fais pas parti de l agence, tuple NON INSERE';
        RETURN NULL;
    END IF; 
    
    RAISE NOTICE 'audtion check => RAS, tuple INSERE';
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER check_insert_audition
BEFORE
INSERT ON audition FOR EACH row
EXECUTE PROCEDURE check_aud();

CREATE OR REPLACE FUNCTION new_management() RETURNS TRIGGER AS $$ 
DECLARE
    liste_demande demande%ROWTYPE;
BEGIN	
    RAISE NOTICE 'Trouver audition pour %', NEW.id_artiste;

	FOR
    liste_demande IN(   
		SELECT * FROM demande WHERE id NOT IN(
            SELECT id_demande FROM audition WHERE id IN(
                SELECT id_audition FROM accepter))

        INTERSECT

        SELECT * FROM demande WHERE id_agence IN
            (SELECT id_agence FROM agent WHERE id IN
                (SELECT DISTINCT id_agent FROM management WHERE id_agent = NEW.id_agent))
        AND metier IN 
            (SELECT nom_metier FROM artiste WHERE id = NEW.id_artiste))
    LOOP
        INSERT INTO audition(id_demande,id_artiste) VALUES (liste_demande.id,NEW.id_artiste);
    END LOOP;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER management_insertion
AFTER
INSERT ON management FOR EACH row
EXECUTE PROCEDURE new_management();