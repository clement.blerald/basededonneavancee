\copy langue(nom) FROM 'CSV/langue.csv' DELIMITER ',' CSV HEADER;

\copy agence(nom,pays,ville) FROM 'CSV/agence.csv' DELIMITER ',' CSV HEADER;

\copy personne(nom,prenom,sex,date_naissance) FROM 'CSV/personne.csv' DELIMITER ',' CSV HEADER;

\copy langue_parle(id_langue,id_personne) FROM 'CSV/langue_parle.csv' DELIMITER ',' CSV HEADER;

\copy agent(id,id_agence) FROM 'CSV/agent.csv' DELIMITER ',' CSV HEADER;

\copy artiste(id_personne,nom_metier) FROM 'CSV/artiste.csv' DELIMITER ',' CSV HEADER;

\copy demande(id_agence,nom_projet,date_demande,metier,deb_periode,fin_periode,sex) FROM 'CSV/demande.csv' DELIMITER ',' CSV HEADER;

\copy management(id_agent,id_artiste) FROM 'CSV/management.csv' DELIMITER ',' CSV HEADER;

\copy langue_demande(langue_id,demande_id) FROM 'CSV/langue_demande.csv' DELIMITER ',' CSV HEADER;
