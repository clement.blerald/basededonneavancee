# BasesDeDonneeAvancees

Projet de Bases de données : Agence Artistique

## Membres du groupe

- Groupe TP : ***mardi***
- Phothirath Anthony : 71702143
- Blérald Clément : 71800092

## Scripts CSV

- les fichiers CSV présent ont été créé génériquement à l'aide d'un site spécialisé. (Mockaroo)
## Architecture

```sh
.
├── CREATION
│   ├── create_all.sql
│   └── create_triggers.sql
├── CSV
│   ├── agence.csv
│   ├── agent.csv
│   ├── artiste.csv
│   ├── demande.csv
│   ├── langue.csv
│   ├── langue_demande.csv
│   ├── langue_parle.csv
│   ├── management.csv
│   ├── nom_demande.csv
│   └── personne.csv
├── insert_data.sql
├── MODELISATION
│   ├── BDA_modelisation.drawioFINALE.pdf
│   └── Modélisation_choix.md
├── README.md
└── TESTS
    ├── test1.sql
    ├── test2.sql
    ├── test3bis.sql
    ├── test3.sql
    └── test4.sql

```