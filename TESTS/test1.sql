\! echo ' ----- DEBUT TEST 1 : ce test se charge de demandé à ce que l artiste demandé parle une langue particulère'
\! echo 'table audition avant : '
SELECT * FROM audition;
\! echo 'On insère dans langue_demande la condition que la demande 45, l artiste doit parler le Russe (5)'
INSERT INTO langue_demande VALUES(5,45);
\! echo 'table audition après : '
SELECT * FROM audition;
\! echo 'Ici seule l artiste 28 parle le Russe, l artiste 13 ne parle pas le Russe il est donc supprime'
SELECT  a.id, lp.id_langue FROM langue_parle lp, artiste a WHERE a.id_personne = lp.id_personne AND (a.id = 28 OR a.id = 13) GROUP BY a.id,lp.id_langue; 
\! echo ' ----- FIN TEST 1 :'