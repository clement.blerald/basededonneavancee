\! echo ' ----- DEBUT TEST 2 : refuser les artistes qui n ont pas réussi l audition  ----- ';
\! echo 'On insère l artiste 11 dans accepter il a passé l audition 13';
\! echo 'table accepter AVANT: ';
SELECT * FROM accepter;
\! echo 'table refuser AVANT: ';
SELECT * FROM refuser;
\! echo 'table realisation AVANT: ';
SELECT * FROM realisation;
\! echo 'table audition : ';
SELECT * FROM audition WHERE id_demande = 41;
\! echo 'Il n y a que les artistes 11 et 32 qui sont concerné par cette demande'
\! echo '--------------------'
INSERT INTO accepter VALUES (13,11);
\! echo '--------------------'
\! echo 'table accepter APRES: ';
SELECT * FROM accepter;
\! echo 'table refuser : APRES';
SELECT * FROM refuser;
\! echo 'table realisation APRES: ';
SELECT * FROM realisation;
\! echo ' ----- FIN TEST 2 ----- ';