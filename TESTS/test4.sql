\! echo ' ----- DEBUT TEST 4 : ce test se charge de demandé à ce que l artiste demandé ai un nom/prenom particulier'
\! echo 'table audition pour la demande d id 30  AVANT : '
SELECT * FROM audition WHERE id_demande = 30;
\! echo 'On insert dans nom_demande l artiste Spalls Fabe pour la demande 30';
INSERT INTO nom_demande VALUES(30,'Spalls','Fabe');
\! echo 'table audition pour la demande d id 30 APRES : '
SELECT * FROM audition where id_demande = 30;
\! echo 'On a enlevé de l audition l artiste qui n etait pas Spalls Fabe';
\! echo ' ----- FIN TEST 4 :'